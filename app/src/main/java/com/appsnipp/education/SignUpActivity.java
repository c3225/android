/*
 * Copyright (c) 2021. rogergcc
 */

package com.appsnipp.education;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {

    EditText t1,t2,t3,t4,t5;
    Button b;
    DBHelper DB;
    TextView v1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        t1=findViewById(R.id.txt_role);
        t2=findViewById(R.id.txt_phone);
        t3=findViewById(R.id.txt_mail);
        t4=findViewById(R.id.txt_passwordIncri);
        t5=findViewById(R.id.txt_confirmePassword);
        b=findViewById(R.id.btn_save);
        v1=findViewById(R.id.loginred);
        v1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(SignUpActivity.this,ConnexionActivity.class);
                startActivity(i);
            }
        });
        DB=new DBHelper(this);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String role=t1.getText().toString();
                String phone=t2.getText().toString();
                String log=t3.getText().toString();
                String pass=t4.getText().toString();
                String confpass=t5.getText().toString();

                if(TextUtils.isEmpty(log) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(confpass))
                    Toast.makeText(SignUpActivity.this,"Mail and Password Required",Toast.LENGTH_SHORT).show();
                else{
                    if(confpass.equals(pass)){
                        Boolean checkUser=DB.checkmail(log);
                        if(checkUser==false){
                            Boolean insert= DB.insertUser(log,pass,phone,role);
                            if(insert==true){
                                Toast.makeText(SignUpActivity.this,"Registered Successfully",Toast.LENGTH_SHORT).show();
                                Intent i=new Intent(SignUpActivity.this,ConnexionActivity.class);
                                startActivity(i);
                            }else{
                                Toast.makeText(SignUpActivity.this,"Registration Failed",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(SignUpActivity.this,"User already Exists",Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(SignUpActivity.this,"password is not confirmed",Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });
    }
}