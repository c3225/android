/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.appsnipp.education.adapter.ActeAdapter;
import com.appsnipp.education.model.Acte;
import com.appsnipp.education.retro.ApiActes;
import com.appsnipp.education.retro.ApiInterface;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActActivity extends AppCompatActivity {
    TextView textError;
    RecyclerView r;
    ProgressDialog pd;
    List<Acte> actList;
    //private Button b1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act);
        textError=findViewById(R.id.textError);
        textError.setVisibility(View.INVISIBLE);
        /*b1=findViewById(R.id.btndon);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater=LayoutInflater.from(ActActivity.this);
                View subView=inflater.inflate(R.layout.donateform,null);
                EditText n=subView.findViewById(R.id.numcard);
                EditText c=subView.findViewById(R.id.code4);
                EditText m=subView.findViewById(R.id.edit_montant);
                AlertDialog.Builder a=new AlertDialog.Builder(ActActivity.this);
                a.setTitle("New Donate");
                a.setView(subView);
                a.create();
                a.setPositiveButton("donate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String num=n.getText().toString();
                        String co =c.getText().toString();
                        String mon =m.getText().toString();
                        Toast.makeText(ActActivity.this, "Your donation is: "+m+"DT", Toast.LENGTH_LONG).show();
                        //startActivity(getIntent());
                    }
                });
                a.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(ActActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
                    }
                });
                a.show();
            }
        });*/
        r=findViewById(R.id.list);
        r.setLayoutManager(new LinearLayoutManager(this));
        pd = new ProgressDialog(this);
        pd.setMessage("Fetching actes...");
        pd.setCancelable(false);
        pd.show();
        try{
            ApiInterface apiService = ApiActes.getActes().create(ApiInterface.class);
            Call<List<Acte>> call=apiService.getActes();
            call.enqueue(new Callback<List<Acte>>() {
                @Override
                public void onResponse(Call<List<Acte>> call, Response<List<Acte>> response) {
                    actList=response.body();
                    //Toast.makeText(ActActivity.this,"list des actes =>"+actList,Toast.LENGTH_LONG).show();
                    ActeAdapter adapter=new ActeAdapter(ActActivity.this,actList);
                    r.setAdapter(adapter);
                    r.smoothScrollToPosition(0);
                    pd.hide();
                }

                @Override
                public void onFailure(Call<List<Acte>> call, Throwable t) {
                    Log.d("TAG", " Response = " + t.toString());
                    Toast.makeText(ActActivity.this, "Error Fetching Data!", Toast.LENGTH_SHORT).show();
                    textError.setVisibility(View.VISIBLE);
                    pd.hide();
                }
            });

        }catch (Exception e){
            Log.d("Error", e.getMessage());
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}