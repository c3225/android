/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education;

public class User {
    private int id;
    private String mail;
    private String password;
    private String role;
    private String phone;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User(String mail, String password, String role, String phone) {
        this.mail = mail;
        this.password = password;
        this.role = role;
        this.phone = phone;
    }
}
