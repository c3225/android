/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education.model;

import com.google.gson.annotations.SerializedName;

public class Acte {
    @SerializedName("titre")
    private String titre;
    @SerializedName("photosUrl")
    private String photoUrl;

    public Acte(String titre, String photoUrl) {
        this.titre = titre;
        this.photoUrl = photoUrl;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
