/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education.retro;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiActes {
    public static String BASE_URL ="http://192.168.1.13:8484/CharityLandRest/";

    private static Retrofit retrofit;
    public static Retrofit getActes(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
