/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education.retro;

import com.appsnipp.education.model.Acte;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("actes")
    Call<List<Acte>> getActes();
}
