/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME= "user_db";
    public static final String TU="user";
    public static final String CID="_id";
    public static final String CP="phone";
    public static final String CM="mail";
    public static final String CPASS="password";
    public static final String CR="role";
    public static final String CREATE_STUDENT_TABLE="CREATE TABLE "+ TU+"("
            +CID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            +CM+" TEXT NOT NULL, "
            +CPASS+" TEXT NOT NULL,"
            +CR+" TEXT NOT NULL,"
            +CP+" TEXT NOT NULL ) ";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_STUDENT_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TU);
        onCreate(db);
    }
    public SQLiteDatabase db;
    public SQLiteDatabase open(){
        db=this.getWritableDatabase();
        return db;
    }
    public Boolean insertUser(String mail,String pass,String phone,String role){
        open();
        ContentValues values =new ContentValues();
        values.put(CM,mail);
        values.put(CPASS,pass);
        values.put(CP,phone);
        values.put(CR,role);
        long resu = db.insert(TU,null, values);
        if(resu==-1) return false;
        else
            return true;
    }
    public Boolean checkmail(String mail){
        open();
        Cursor c= db.rawQuery("select * from "+TU+" where mail=?",new String[] {mail});
        if (c.getCount()>0){
            return true;
        }else
            return false;
    }
    public Boolean checkmailandpass(String mail,String password){
        open();
        Cursor c= db.rawQuery("select * from "+TU+" where mail=? and password=?",new String[] {mail,password});
        if (c.getCount()>0){
            return true;
        }else
            return false;
    }

}
