/*
 * Copyright (c) 2021. rogergcc
 */

package com.appsnipp.education;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ConnexionActivity extends AppCompatActivity {
    //String login="hamzus";
    //String password="hamza123";
    EditText e1,e2;
    Button bx,bi,bp;
    DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        bx=findViewById(R.id.btn_connect);
        e1=findViewById(R.id.txt_loginn);
        e2=findViewById(R.id.txt_password);
        bi=findViewById(R.id.btn_inscri);
        DB=new DBHelper(this);
        bi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ConnexionActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });
        bx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String log=e1.getText().toString();
                String pass=e2.getText().toString();

                if(TextUtils.isEmpty(log) ||TextUtils.isEmpty(pass)){
                    Toast.makeText(ConnexionActivity.this,"Mail and Password Required",Toast.LENGTH_SHORT).show();
                }
                else{
                    Boolean checkUserpass=DB.checkmailandpass(log,pass);
                    if (checkUserpass==true){
                        Toast.makeText(ConnexionActivity.this,"login success",Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ConnexionActivity.this,MainActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(ConnexionActivity.this,"Mail or Password incorrect",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}