/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appsnipp.education.R;
import com.appsnipp.education.model.Acte;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ActeAdapter extends RecyclerView.Adapter<ActeViewHolder> {
    Context context;
    List<Acte> list;

    public ActeAdapter(Context context, List<Acte> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ActeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_popular_courses,parent,false);
        return new ActeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActeViewHolder holder, int position) {
        holder.titre.setText(list.get(position).getTitre());
        Picasso.with(context).load(list.get(position).getPhotoUrl())
                .placeholder(R.drawable.load)
                .into(holder.photoUrl);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
