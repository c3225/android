/*
 * Copyright (c) 2022. rogergcc
 */

package com.appsnipp.education.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appsnipp.education.R;

public class ActeViewHolder extends RecyclerView.ViewHolder{
    TextView titre;
    ImageView photoUrl;

    public ActeViewHolder(@NonNull View itemView) {
        super(itemView);
        titre=itemView.findViewById(R.id.tit);
        photoUrl=itemView.findViewById(R.id.image);
    }
}
